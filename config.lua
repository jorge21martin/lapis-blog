local config = require("lapis.config")

config("development", {
	postgres = {
		host = "127.0.0.1",
		user = "bear",
		password = "2bt3vnvy",
		database = "blog"
	}
})
