local lapis      = require("lapis")
local respond_to = require("lapis.application").respond_to

local app = lapis.Application()
app:enable("etlua")
app.layout = require("views.layout")

local Post = require("controllers.post")
local Auth = require("controllers.auth")

app:get("home", "/", function(self)
	return Post.page(self) 
end)

app:match("post", "/post/:title", function(self)
	return Post.read(self)
end)

app:match("delete", "/delete/:id", function(self)
	return Post.delete(self)
end)

app:match("write", "/write", respond_to({
	GET = function(self)
		if self.session.user then
			return { render = "write" }
		else
			return "<h2>You must be logged in to create a post</h2>"
		end	
	end,

	POST = function(self)
		return Post.create(self)
	end
}))

app:match("edit", "/edit/:id", respond_to({
	before = function(self)
		app.page_title = "Jorge Martin | Edit"
	end,	

	GET = function(self)
		return Post.edit.get(self)
	end,

	POST = function(self)
		return Post.edit.post(self)
	end	
}))

app:match("login", "/login", respond_to({
	GET = function(self)
		if loggedin then
			return { render = "logout" }
		end

		return { render = "login" }
	end,

	POST = function(self)
		if Auth.login(self) then
			self.session.user = self.params.username
		
			return "<h2>Logged in<h2>"
		else
			return "<h2>Login Incorrect</h2>"
		end	
	end
}))

app:match("/register", respond_to({
	GET = function(self)
		return { render = "register" }
	end,

	POST = function(self)
		return Auth.register(self)
	end
}))

app:match("logout", "/logout", function(self)
	self.session.user = nil	
	self.loggedin = nil

	return { redirect_to = self:url_for("home") }
end)

return app
