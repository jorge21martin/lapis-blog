local user = {}

user.is_logged_in = function(app)
	if app.session.user then
		return true
	end
	
	return false
end

return user
