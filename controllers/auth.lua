local crypt = require("lsha2")
local Models = require("models")

local Auth = {}

local function ranval()
        local frandom = assert(io.open("/dev/urandom", "rb"))
        local s = frandom:read(4)
        assert(s:len() == 4)
        local v = 0
        for i = 1, 4 do
                v = 256 * v + s:byte(i)
        end
        return v
end

Auth.login = function(app)
	local name = app.params.username
	local password = app.params.password
	local response = Models.user:find(name)

	if not response then
		return nil
	end

        --Hash input
        local password = password .. response.salt
        password = crypt.hash256(password)

        if response.hash == password then
		return true
	end

	return nil
end

Auth.register = function(app)
	local username = app.params.username
	local password = app.params.password	

	--Salt Password
        local salt = ranval()
        password = password .. salt

        --Hash it
        local hash = crypt.hash256(password)

        local user = Models.user:create({
                username = username,
                hash = hash,
                salt = salt
        })

	return "Created"
end

return Auth
