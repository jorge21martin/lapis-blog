local Post = {}

local Models = require("models")
local validate = require("lapis.validate")
local db = require("lapis.db")
local markdown = require("markdown")

--Show a post
Post.read = function(app)
	--Get post from database	
	local post = Models.post:find(app.params.title)	

	--Pass information to view
	app.id     = post.id	
	app.title  = post.title
	app.author = post.author
	app.date   = post.date
	app.body   = markdown(post.body)

	--Set title
	app.page_title = "Jorge Martin | " .. post.title

	return { render = "post" }
end

Post.page = function(app)
	--Get the ten most recent posts
	local query = "* FROM posts order by id desc limit 10"
	app.posts = db.select(query)	

	return { render = "home" }
end

Post.write = function(app)
	if not app.session.user then
		app.page_title = "Jorge Martin | Write"

		return { render = "write" }

	end

	return "<h2>You must be logged in to create a post</h2>"
end

Post.create = function(app)
	--Set Page title
	app.page_title = "Jorge Martin | Write"

	--Make sure user is logged in
	if not app.session.user then
		return "<h2>You must log in to create a post</h2>"
	end

	local date = db.format_date()
	
	--Create the post
	local post = Models.post:create({
		title       = app.params.title,
		author      = app.params.author,
		date        = date,
		description = app.params.description,
		body        = app.params.body
	})

	return { redirect_to = app:url_for("post", {title=post.id}) }
end

Post.edit = {}

Post.edit.get = function(app)
	app.page_title = "Jorge Martin | Edit"

	--Check user is logged in 
	if not app.session.user then
		return "<h2>You must be logged in to edit a post</h2>"
	end

	--Get post from database
	local post = Models.post:find(app.params.id)	

	--Give params to view
	app.post = post

	return { render = "edit" }
end

Post.edit.post = function(app)
	--Find model	
	local post = Models.post:find(app.params.id)

	--Update information
	post.title = app.params.title
	post.author = app.params.author
	post.description = app.params.description
	post.body = app.params.body

	post:update("title", "author", "description", "body")		

	return { redirect_to = app:url_for("post", {title=app.params.id}) }
end

Post.delete = function(app)
	local post = Models.post:find(app.params.id)
	post:delete()

	return { redirect_to = app:url_for("home") }
end

return Post
