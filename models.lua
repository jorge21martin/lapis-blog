local Model = require("lapis.db.model").Model
local Models = {}

Models.post = Model:extend("posts", {
	primary_key = "id"
})

Models.user = Model:extend("users", {
	primary_key = "username"
})

return Models 
