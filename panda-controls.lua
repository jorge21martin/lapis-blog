#!/usr/bin/lua
--Panda Controls
--Programmer: digipanda

control_dir    = "controllers/"
controls = {}

--Create a filename for inputs
function makeFileName(name)
	local suffix = ".lua"
	local output = control_dir .. name .. suffix

	return output	
end

--Create file contents
function makeContent(cname)
	local output = [[
local title = {}

title.index = function(app)
	return nil
end

return title
]]
	
	output = string.gsub(output, "title", cname)	

	return output
end

--Write to file
function writeFile(name, content)
	--Create the file
	local file = io.open(name, "w")
	file:write(content)
	file:flush()	
	file:close()
end

--Handle args
for k, v in ipairs(arg) do
	if k >= 1 then
		--Append file names to controls
		local name = makeFileName(v)
		controls[v] = name	
	end
end

--Create files
for k, v in pairs(controls) do
	local content = makeContent(k)
	writeFile(v, content)
end







